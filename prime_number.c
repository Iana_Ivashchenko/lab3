#include <stdio.h>

int is_prime(unsigned long a){
	unsigned long i;
	if (a==2) return 1;
	if (a<=1 || a%2==0) return 0;
	for(i=3;i*i<=a;i+=2){
		if (a%i==0) return 0;
	}
	return 1;
}

int main(int args,char** argv){
	unsigned long number;
	char symbol;
	puts("Type your number:");
	while(scanf("%lu%c", &number, &symbol) != 2 && symbol != '\n') {/*scanf возвращает количество параметров, которое было считано*/
		scanf("%*s");/*если параметры были считаны как-то неверно, то scanf не был выполнен и будет продолжаться, выдавая неверные данные. Это функция считывания до конца строки*/
		puts("Type your number:");
	}
	printf("Your number is %lu. It is %s.\n",number,is_prime(number)? "prime":"not prime");
	return 0;
}
