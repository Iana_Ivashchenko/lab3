#include <stdio.h>

const int a_vector[]={0,8,6,-7,9,2147483647}, b_vector[]={3,89,-5,4,642,2};

long scalar_product(const int* a, const int* b, size_t count_array){
	size_t i=0;
	long sum=0;
	for (i=0;i<count_array;i++)
		sum+=(long)a[i]*b[i];
	return sum;
}

int main(int args,char** argv){
	size_t array_length=sizeof(b_vector)/sizeof(b_vector[0]);
	printf("The scalar product of arrays is: %li\n",scalar_product(a_vector,b_vector,array_length));
	return 0;
}